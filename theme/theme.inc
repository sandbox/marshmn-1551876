<?php
/**
 * @file
 */


/**
 * theme_egd_player_options_str()
 */
function theme_egd_player_options_str($variables) {
  $player = $variables['player'];

  $output = t('@pin: @firstname @lastname - @country: @club', array(
    '@pin' => $player['PIN'],
    '@firstname' => $player['First_Name'],
    '@lastname' => $player['Last_Name'],
    '@country' => $player['Country_Code'],
    '@club' => $player['Club'],
  ));

  return $output;
}
