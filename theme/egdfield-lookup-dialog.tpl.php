<?php
/**
 * @file
 *
 * Theme template for the player lookup modal dialog (ctools modal).
 */
?>

<div id="ctools-modal" class="popups-box">
  <div class="ctools-modal-content egdfield-lookup-dialog-content">
    <table cellpadding="0" cellspacing="0" id="ctools-face-table">
      <tr>
        <td class="popups-tl popups-border"></td>
        <td class="popups-t popups-border"></td>
        <td class="popups-tr popups-border"></td>
      </tr>
      <tr>
        <td class="popups-cl popups-border"></td>
        <td class="popups-c" valign="top">
          <div class="popups-container">
            <div class="modal-header popups-title">
              <span id="modal-title" class="modal-title"></span>
              <div class="clear-block"></div>
            </div>
            <div class="modal-scroll"><div id="modal-content" class="modal-content popups-body"></div></div>
            <div class="popups-buttons"></div>
            <div class="popups-footer"></div>
          </div>
        </td>
        <td class="popups-cr popups-border"></td>
      </tr>
      <tr>
        <td class="popups-bl popups-border"></td>
        <td class="popups-b popups-border"></td>
        <td class="popups-br popups-border"></td>
      </tr>
    </table>
  </div>
</div>
