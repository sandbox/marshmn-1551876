<?php

/**
 * @file
 */


/**
 * egdfield_egd_get_player()
 */
function egdfield_egd_get_player($pin) {
  $egd_json = file_get_contents(egdfield_egd_get_url('ws_player') . '?pin=' . $pin);

  if ($egd_json == FALSE) {
    return FALSE;
  }

  $egd_obj = drupal_json_decode($egd_json);

  if (is_null($egd_obj)) {
    return FALSE;
  }

  $retcode = $egd_obj{'retcode'};

  if ($retcode != 'Ok') {
    return FALSE;
  }

  $player = array();

  $player['PIN'] = $egd_obj{'Pin_Player'};
  $player['First_Name'] = $egd_obj{'Name'};
  $player['Last_Name'] = $egd_obj{'Last_Name'};
  $player['Country_Code'] = $egd_obj{'Country_Code'};
  $player['Club'] = $egd_obj{'Club'};
  $player['Grade'] = $egd_obj{'Grade'};

  return $player;
}

/**
 * egdfield_egd_get_players()
 *
 * Returns an array of players from the EGD who match (fully or whole) the parameters.
 *
 */
function egdfield_egd_get_players($lastname = NULL, $firstname = NULL) {
  $query = array();

  if (!is_null($lastname) && $lastname != '') {
    $query[] = 'lastname=' . urlencode($lastname);
  }

  if (!is_null($firstname) && $firstname != '') {
    $query[] = 'name=' . urlencode($firstname);
  }

  return egdfield_egd_get_players_by_query($query);
}

/**
 * egdfield_egd_get_players_by_query()
 */
function egdfield_egd_get_players_by_query($query) {
  if (empty($query)) {
    return array();
  }

  $egd_json = file_get_contents(egdfield_egd_get_url('ws_player_search') . '?' . implode('&', $query));

  if ($egd_json == FALSE) {
    return FALSE;
  }

  $egd_obj = drupal_json_decode($egd_json);

  if (is_null($egd_obj)) {
    return FALSE;
  }

  $retcode = $egd_obj{'retcode'};

  if ($retcode == 'not found') {
    return array();
  }

  if ($retcode != 'Ok') {
    return FALSE;
  }

  $players = array();
  $player_objs = $egd_obj{'players'};

  foreach ($player_objs as $player_obj) {
    $player = array();

    $player['PIN'] = $player_obj{'Pin_Player'};
    $player['First_Name'] = $player_obj{'Name'};
    $player['Last_Name'] = $player_obj{'Last_Name'};
    $player['Country_Code'] = $player_obj{'Country_Code'};
    $player['Club'] = $player_obj{'Club'};
    $player['Grade'] = $player_obj{'Grade'};

    $players[$player['PIN']] = $player;
  }

  return $players;
}

/**
 * egdfield_egd_get_player_page_url()
 */
function egdfield_egd_get_player_page_url($pin) {
  return egdfield_egd_get_url('player_page') . '?key=' . $pin;
}

/**
 * egdfield_egd_get_player_graph_url()
 */
function egdfield_egd_get_player_graph_url($pin, $small) {
  $url = egdfield_egd_get_url('player_graph') . '?Pin0=' . $pin;

  if ($small == TRUE) {
    $url .= '&Size=small';
  }
  else {
    $url .= '&Title=' . urlencode(t('GoR vs rank progression'));
  }

  return $url;
}

/**
 *  egdfield_egd_get_url()
 */
function egdfield_egd_get_url($type) {
  $base_url = 'http://www.europeangodatabase.eu/EGD';

  switch($type) {
    case 'player_page':
      return $base_url . '/Player_Card.php';

    case 'player_graph':
      return $base_url . '/EGD_2_0/newChart.php';

    case 'ws_player_search':
      return $base_url . '/GetPlayerDataByData.php';

    case 'ws_player':
      return $base_url . '/GetPlayerDataByPIN.php';
  }

  return FALSE;
}
