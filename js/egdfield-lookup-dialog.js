/**
* @file
*
* Lookup dialog ctools modal theme.
*/
Drupal.theme.prototype.EGDFieldLookupModal = function () {
  return Drupal.CTools.Modal.currentSettings.modalThemeHTML;
}
