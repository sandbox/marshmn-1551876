<?php

/**
 * @file
 *
 * Functions related to the looking up of players.
 */

/**
 * egdfield_lookup()
 *
 * Page callback for the /egdfield/%ctools_js/lookup menu item.
 */
function egdfield_lookup($js = NULL, $field_name = NULL) {
  if (!$js) {
    return drupal_det_form('egdfield_lookup_form');
  }

  ctools_include('ajax');
  ctools_include('modal');

  $form_state = array(
    'ajax' => TRUE,
    'title' => t('EGD Player Lookup'),
    'values' => array(
      'orig_field_name' => $field_name,
    ),
  );

  $output = ctools_modal_form_wrapper('egdfield_lookup_form', $form_state);

  if (!empty($form_state['executed'])) {
    $selected_player = $form_state['values']['players'];

    $commands = array();

    if (substr($form_state['clicked_button']['#id'],0,11) == 'edit-submit' ) {
      $commands[] = ajax_command_invoke('#' . $form_state['values']['orig_field_name'], 'val', array($selected_player));
    }
    
    $commands[] = ctools_modal_command_dismiss(t('Done'));

    print ajax_render($commands);
    exit;
  }

  print ajax_render($output);
  exit;
}

/**
 * egdfield_lookup_form()
 */
function egdfield_lookup_form($form, &$form_state) {
  $lastname = $form_state['values']['lastname'];

  $form['orig_field_name'] = array(
    '#type' => 'hidden',
    '#value' => isset($form_state['values']['orig_field_name'])?$form_state['values']['orig_field_name']:'',
  );
  $form['lastname'] = array(
    '#type' => 'textfield',
    '#title' => t('Last Name'),
    '#description' => t('Player\'s last name (or part of) to lookup in the EGD.'),
    '#size' => 40,
    '#maxlength' => 120,
    '#ajax' => array(
      'callback' => 'egdfield_perform_lookup',
      'wrapper' => 'egdfield_lookup_wrapper',
    ),
  );
  $form['players'] = array(
    '#type' => 'select',
    '#title' => t('Players'),
    '#options' => egdfield_egd_players_options_array($lastname),
    '#size' => 10,
    '#description' => t('Select the desired EGD player.'),
    '#prefix' => '<div id="egdfield_lookup_wrapper">',
    '#suffix' => '</div>',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Use Selected'),
  );
  $form['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
  );
  return $form;
}

/**
 * egdfield_egd_players_options_array()
 */
function egdfield_egd_players_options_array($lastname) {
  $players = egdfield_egd_get_players($lastname);

  if ($players === FALSE) {
    return array('1' => t('Error: Unable to lookup players'));
  }

  $players_options = array();

  foreach ($players as $player_pin => $player) {
    $players_options[$player_pin] = theme('egd_player_options_str', array('player' => $player));
  }

  return $players_options;
}

/**
 * egdfield_perform_lookup()
 */
function egdfield_perform_lookup($form, &$form_state) {
  return $form['players'];
}

/**
 * _lookup_dialog_init()
 *
 * Initialization of the lookup popup dialog.
 */
function _lookup_dialog_init() {
  ctools_include('ajax');
  ctools_include('modal');

  ctools_modal_add_js();

  $lookup_dialog_style = array(
    'egdfield-lookup-dialog-style' => array(
      'modalSize' => array(
        'type' => 'fixed',
        'width' => 600,
        'height' => 400,
        'addWidth' => 20,
        'addHeight' => 15,
      ),
      'modalOptions' => array(
        'opacity' => .5,
        'background-color' => '#000',
      ),
      'animation' => 'fadeIn',
      'modalTheme' => 'EGDFieldLookupModal',
      'modalThemeHTML' => theme('lookup_dialog', array()),
      'throbber' => theme('image', array(
        'path' => ctools_image_path('ajax-loader.gif', 'egdfield'),
        'alt' => t('Loading...'),
        'title' => t('Loading'),
      )),
    ),
  );

  drupal_add_js($lookup_dialog_style, 'setting');

  ctools_add_js('egdfield-lookup-dialog', 'egdfield');
  ctools_add_css('egdfield-lookup-dialog', 'egdfield');

  return;
}
