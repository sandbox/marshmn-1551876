<?php

/**
 * @file
 * Provides field widget hooks for egdfield module.
 */

/**
 * Implements hook_field_widget_info().
 */
function egdfield_field_widget_info() {
  $widgets = array();

  $widgets['egdfield_pin'] = array(
    'label' => t('Text field'),
    'field types' => array('egdfield'),
    'behaviors' => array(
      'multiple values' => FIELD_BEHAVIOR_DEFAULT,
      'default value' => FIELD_BEHAVIOR_DEFAULT,
    ),
  );

  return $widgets;
}

/**
 * Implements hook_field_widget_form().
 */
function egdfield_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  $base = $element;

  switch ($instance['widget']['type']) {
    case 'egdfield_pin':
      $pin_field_id = 'egdfield-pin-' . $instance['field_name'] . '-' . $delta;
      $lookup_button_id = 'egdfield-lookup-button-' . $instance['field_name'] . '-' . $delta;
      
      $element['pin'] = array(
        '#type' => 'textfield',
        '#title' => t('European Go Database PIN'),
        '#default_value' => isset($items[$delta]['pin']) ? $items[$delta]['pin'] : NULL,
        '#id' => $pin_field_id,
        '#ajax' => array(
          'callback' => 'egdfield_pin_callback',
        ),
      ) + $base;
      $element['update_field_firstname'] = array(
        '#type' => 'hidden',
        '#default_value' => $instance['settings']['update_field_firstname'],
      ) + $base;
      $element['update_field_lastname'] = array(
        '#type' => 'hidden',
        '#default_value' => $instance['settings']['update_field_lastname'],
      ) + $base;
      $element['update_field_countrycode'] = array(
        '#type' => 'hidden',
        '#default_value' => $instance['settings']['update_field_countrycode'],
      ) + $base;
      $element['update_field_club'] = array(
        '#type' => 'hidden',
        '#default_value' => $instance['settings']['update_field_club'],
      ) + $base;
      $element['lookup_url'] = array(
        '#type' => 'hidden',
        '#attributes' => array('class' => array($lookup_button_id . '-url', 'ctools-modal-egdfield-lookup-dialog-style')),
        '#value' => url('egdfield/nojs/lookup/' . $pin_field_id),
      ) + $base;
      $element['lookup_button'] = array(
        '#type' => 'button',
        '#value' => 'Lookup...',
        '#attributes' => array('class' => array('ctools-modal-egdfield-lookup-dialog-style', 'ctools-use-modal')),
        '#id' => $lookup_button_id,
      ) + $base;

      _lookup_dialog_init();

      break;
  }

  return $element;
}

/**
 * egdfield_pin_callback()
 */
function egdfield_pin_callback($form, &$form_state) {
  $trigger_field = $form_state['triggering_element']['#field_name'];

  $player_pin = $form_state['values'][$trigger_field]['und']['0']['pin'];
  $player = egdfield_egd_get_player($player_pin);

  $update_fields = array(
    'update_field_firstname' => 'First_Name',
    'update_field_lastname' => 'Last_Name',
    'update_field_countrycode' => 'Country_Code',
    'update_field_club' => 'Club',
  );

  $commands = array();

  foreach ($update_fields as $update_field_name => $player_entry) {
    $field = $form_state['values'][$trigger_field]['und']['0'][$update_field_name];

    if ($field != '') {
      $field_id = '#' . $form[$field]['und']['0']['#id'] . '-value';
      $commands[] = ajax_command_invoke($field_id, 'val', array($player[$player_entry]));
    }
  }

  print ajax_render($commands);
  exit;
}
