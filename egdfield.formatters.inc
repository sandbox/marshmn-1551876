<?php

/**
 * @file
 * Drupal field formatter hooks and helper functions.
 */

/**
 * Implements hook_field_formatter_info().
 */
function egdfield_field_formatter_info() {
  $formatters = array(
    'egdfield_text' => array(
      'label' => t('Text'),
      'field types' => array('egdfield'),
      'settings' => array('link' => TRUE),
    ),
    'egdfield_graph' => array(
      'label' => t('Graph'),
      'field types' => array('egdfield'),
      'settings' => array(
        'link' => TRUE,
        'small' => FALSE,
      ),
    ),
  );

  return $formatters;
}

/**
 * Implements hook_field_formatter_view().
 */
function egdfield_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();

  $settings = $display['settings'];

  switch ($display['type']) {
    case 'egdfield_text':
      foreach ($items as $delta => $item) {
        $output = t('@pin',array(
          '@pin' => $item['pin'],
        ));

        if ($settings['link'] == TRUE) {
          $link = egdfield_egd_get_player_page_url($item['pin']);
          $output = l($output,$link);
        }

        $element[$delta] = array('#markup' => $output);
      }

      break;
    case 'egdfield_graph':
      foreach ($items as $delta => $item) {
        $output = theme_image(array(
          'path' => egdfield_egd_get_player_graph_url($item['pin'], $settings['small']),
          'alt' => t('EGD ratings graph'),
          'attributes' => NULL,
        ));

        if ($settings['link'] == TRUE) {
          $link = egdfield_egd_get_player_page_url($item['pin']);
          $output = l($output,$link,array('html'=>TRUE));
        }

        $element[$delta] = array('#markup' => $output);
      }

      break;
  }

  return $element;
}

/**
 * egdfield_field_formatter_settings_form()
 */
function egdfield_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];
  $form = array();

  if ($display['type'] == 'egdfield_graph') {
    $form['small'] = array(
      '#title' => t('Small graph'),
      '#type' => 'checkbox',
      '#default_value' => $settings['small'],
    );
  }

  if ($display['type'] == 'egdfield_text' || $dispaly['type'] == 'egdfield_graph') {
    $form['link'] = array(
      '#title' => t('Link to EGD player page'),
      '#type' => 'checkbox',
      '#default_value' => $settings['link'],
    );
  }

  return $form;
}

/**
 * egdfield_field_formatter_settings_summary()
 */
function egdfield_field_formatter_settings_summary($field, $instance, $view_mode) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];
  $summary = array();

  if ($display['type'] == 'egdfield_graph') {
    $item = t('Size: ');
    $item .= $settings['small'] == TRUE ? t('Small') : t('Large');

    $summary[] = $item;
  }

  if ($display['type'] == 'egdfield_text' || $display['type'] == 'egdfield_graph') {
    $item = t('Link to EGD player page: ');
    $item .= $settings['link'] == TRUE ? t('Yes') : t('No');

    $summary[] = $item;
  }

  return theme_item_list(array(
    'items' => $summary,
    'title' => NULL,
    'attributes' => array(),
    'type' => 'ul',
  ));
}
